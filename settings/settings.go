package settings

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/veandco/go-sdl2/sdl"
	"gopkg.in/yaml.v2"
)

type Settings struct {
	Keyboard *Keyboard `yaml:"keyboard"`
	Gamepad  *Gamepad  `yaml:"gamepad"`
	Mouse    *Mouse    `yaml:"mouse"`
}

type Keyboard struct {
	Up    sdl.Scancode `yaml:"up"`
	Left  sdl.Scancode `yaml:"left"`
	Down  sdl.Scancode `yaml:"down"`
	Right sdl.Scancode `yaml:"right"`
	Fire  sdl.Scancode `yaml:"fire"`
	Power sdl.Scancode `yaml:"power"`
	Use   sdl.Scancode `yaml:"use"`
	Swap  sdl.Scancode `yaml:"swap"`
}

type Gamepad struct {
	Up    uint8 `yaml:"up"`
	Left  uint8 `yaml:"left"`
	Down  uint8 `yaml:"down"`
	Right uint8 `yaml:"right"`
	Fire  uint8 `yaml:"fire"`
	Power uint8 `yaml:"power"`
	Use   uint8 `yaml:"use"`
	Swap  uint8 `yaml:"swap"`
	HAxis uint8 `yaml:"haxis"`
	VAxis uint8 `yaml:"vaxis"`
}

type Mouse struct {
	Fire  uint8 `yaml:"fire"`
	Power uint8 `yaml:"power"`
	Use   uint8 `yaml:"use"`
	Swap  uint8 `yaml:"swap"`
}

var Global *Settings

func LoadSettings(filename string) {
	s := Settings{
		Keyboard: &Keyboard{
			Up:    sdl.SCANCODE_W,
			Left:  sdl.SCANCODE_A,
			Down:  sdl.SCANCODE_S,
			Right: sdl.SCANCODE_D,
			Fire:  0xFFFFFFFF,
			Power: 0xFFFFFFFF,
			Use:   sdl.SCANCODE_E,
			Swap:  sdl.SCANCODE_Q,
		},
		Gamepad: &Gamepad{
			Up:    sdl.CONTROLLER_BUTTON_DPAD_UP,
			Left:  sdl.CONTROLLER_BUTTON_DPAD_LEFT,
			Down:  sdl.CONTROLLER_BUTTON_DPAD_DOWN,
			Right: sdl.CONTROLLER_BUTTON_DPAD_RIGHT,
			Fire:  sdl.CONTROLLER_BUTTON_RIGHTSHOULDER,
			Power: sdl.CONTROLLER_BUTTON_LEFTSHOULDER,
			Use:   sdl.CONTROLLER_BUTTON_A,
			Swap:  sdl.CONTROLLER_BUTTON_B,
			HAxis: sdl.CONTROLLER_AXIS_RIGHTX,
			VAxis: sdl.CONTROLLER_AXIS_RIGHTY,
		},
		Mouse: &Mouse{
			Fire:  sdl.BUTTON_LEFT,
			Power: sdl.BUTTON_RIGHT,
			Use:   sdl.BUTTON_X1,
			Swap:  sdl.BUTTON_X2,
		},
	}

	body, err := ioutil.ReadFile(filename)
	if err == nil {
		err = yaml.Unmarshal(body, &s)
		if err != nil {
			log.Fatalf("Could not unmarshal yaml: %v", err)
		}
	} else {
		log.Printf("Config file error (%v) - using defaults", err)
	}

	Global = &s
}

func SaveSettings(filename string) error {
	data, err := yaml.Marshal(Global)
	if err != nil {
		log.Printf("Could not marshal yaml: %v", err)
		return err
	}

	err = ioutil.WriteFile(filename, data, 0644)
	if err != nil {
		log.Printf("Could not write settings: %v", err)
		return err
	}

	fmt.Println("Settings saved")
	return nil
}
