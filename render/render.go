package render

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strings"

	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/veandco/go-sdl2/sdl"
)

var window *sdl.Window

var indexBuffer uint32
var indexData []uint32

var zIndex float32

var cameraX float32
var cameraY float32

type BlendMode int

const (
	NoBlend BlendMode = iota
	AdditiveBlend
	SubtractiveBlend
	AlphaBlend
	NumBlendModes
)

type Texture struct {
	tex   uint32
	vao   uint32
	vbo   uint32
	verts []float32
}

var textures [NumBlendModes][]*Texture

var shaderProg uint32

// CompileShader
func CompileShader(source string, kind uint32) (uint32, error) {
	text, err := ioutil.ReadFile(source)
	if err != nil {
		panic(err)
	}

	src, freefn := gl.Strs(string(text) + "\x00")
	defer freefn()

	shader := gl.CreateShader(kind)
	gl.ShaderSource(shader, 1, src, nil)
	gl.CompileShader(shader)

	var success int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &success)

	if success == gl.FALSE {
		var loglength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &loglength)

		log := gl.Str(strings.Repeat("\x00", int(loglength)))
		gl.GetShaderInfoLog(shader, loglength, nil, log)

		return 0, errors.New(gl.GoStr(log))
	}

	return shader, nil
}

func LinkProgram(vert uint32, frag uint32) (uint32, error) {
	prog := gl.CreateProgram()
	gl.AttachShader(prog, vert)
	gl.AttachShader(prog, frag)
	gl.LinkProgram(prog)

	var success int32
	gl.GetProgramiv(prog, gl.LINK_STATUS, &success)

	if success == gl.FALSE {
		var loglength int32
		gl.GetProgramiv(prog, gl.INFO_LOG_LENGTH, &loglength)

		log := gl.Str(strings.Repeat("\x00", int(loglength)))
		gl.GetProgramInfoLog(prog, loglength, nil, log)

		return 0, fmt.Errorf("%s", gl.GoStr(log))
	}

	return prog, nil
}

func CheckError() {
	glError := gl.GetError()

	if glError != gl.NO_ERROR {
		panic(fmt.Sprintf("%d", glError))
	}
}

func Init(width int32, height int32) {
	var err error

	err = sdl.Init(sdl.INIT_EVERYTHING)
	if err != nil {
		panic(err)
	}

	_ = sdl.GLSetAttribute(sdl.GL_CONTEXT_MAJOR_VERSION, 3)
	_ = sdl.GLSetAttribute(sdl.GL_CONTEXT_MINOR_VERSION, 3)

	println("Create window")

	window, err = sdl.CreateWindow("3 1 2 Panic!", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED, width, height, sdl.WINDOW_SHOWN|sdl.WINDOW_OPENGL)
	if err != nil {
		panic(err)
	}

	context, err := window.GLCreateContext()
	if err != nil {
		panic(err)
	}

	err = window.GLMakeCurrent(context)
	if err != nil {
		panic(err)
	}

	println("GL init")

	err = gl.Init()
	if err != nil {
		panic(err)
	}

	println("Compiling shaders")

	vert, err := CompileShader("assets/glsl/vert.glsl", gl.VERTEX_SHADER)
	if err != nil {
		panic(err)
	}

	frag, err := CompileShader("assets/glsl/frag.glsl", gl.FRAGMENT_SHADER)
	if err != nil {
		panic(err)
	}

	prog, err := LinkProgram(vert, frag)
	if err != nil {
		panic(err)
	}

	gl.UseProgram(prog)

	gl.ActiveTexture(gl.TEXTURE0)
	gl.Uniform1i(gl.GetUniformLocation(prog, gl.Str("tex\x00")), 0)

	shaderProg = prog

	println("Setup viewport")

	gl.Viewport(0, 0, width, height)

	gl.ClearColor(0.2, 0.2, 0.2, 1.0)

	gl.Enable(gl.BLEND)
	gl.Enable(gl.DEPTH_TEST)

	gl.GenBuffers(1, &indexBuffer)

	var i uint32

	for i = 0; i < 1024*1024; i += 4 {
		indexData = append(indexData, i, i+1, i+2, i+2, i+3, i)
	}

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(indexData)*4, gl.Ptr(indexData), gl.STATIC_DRAW)

	CheckError()
}

func LoadTexture(surf *sdl.Surface, blend BlendMode) *Texture {
	var tex uint32
	gl.GenTextures(1, &tex)
	gl.BindTexture(gl.TEXTURE_2D, tex)

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)

	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	gl.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)

	gl.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, surf.W, surf.H, 0, gl.RGBA, gl.UNSIGNED_BYTE, gl.Ptr(surf.Pixels()))

	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)

	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, 8*4, nil)
	gl.EnableVertexAttribArray(0)

	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, 8*4, gl.PtrOffset(3*4))
	gl.EnableVertexAttribArray(1)

	gl.VertexAttribPointer(2, 3, gl.FLOAT, false, 8*4, gl.PtrOffset(5*4))
	gl.EnableVertexAttribArray(2)

	result := &Texture{
		tex: tex,
		vao: vao,
		vbo: vbo,
	}

	textures[blend] = append(textures[blend], result)

	return result
}

func (t *Texture) DrawSimpleQuad(x1 float32, y1 float32, x2 float32, y2 float32) {
	t.DrawQuad(x1, y1, x2, y2, 0.0, 0.0, 1.0, 1.0)
}

func (t *Texture) DrawSimpleQuadColor(x1 float32, y1 float32, x2 float32, y2 float32, color sdl.Color) {
	t.DrawQuadColor(x1, y1, x2, y2, 0.0, 0.0, 1.0, 1.0, color)
}

func (t *Texture) DrawQuad(x1 float32, y1 float32, x2 float32, y2 float32, u1 float32, v1 float32, u2 float32, v2 float32) {
	t.DrawQuadColor(x1, y1, x2, y2, u1, v1, u2, v2, sdl.Color{255, 255, 255, 255})
}

func (t *Texture) DrawQuadColor(x1 float32, y1 float32, x2 float32, y2 float32, u1 float32, v1 float32, u2 float32, v2 float32, color sdl.Color) {
	zIndex = zIndex - 0.00001

	var r, g, b float32

	r = float32(color.R) / 255.0
	g = float32(color.G) / 255.0
	b = float32(color.B) / 255.0

	t.verts = append(t.verts, x1-cameraX, y1-cameraY, zIndex, u1, v1, r, g, b)
	t.verts = append(t.verts, x2-cameraX, y1-cameraY, zIndex, u2, v1, r, g, b)
	t.verts = append(t.verts, x2-cameraX, y2-cameraY, zIndex, u2, v2, r, g, b)
	t.verts = append(t.verts, x1-cameraX, y2-cameraY, zIndex, u1, v2, r, g, b)
}

func SetCamera(x int32, y int32) {
	cameraX = float32(x) / 160.0
	cameraY = float32(y) / 120.0
}

func Clear() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)
}

func Swap() {
	var blend BlendMode

	for blend = NoBlend; blend < NumBlendModes; blend++ {
		switch blend {
		case NoBlend:
			gl.BlendEquation(gl.FUNC_ADD)
			gl.BlendFunc(gl.ONE, gl.ZERO)
		case AdditiveBlend:
			gl.BlendEquation(gl.FUNC_ADD)
			gl.BlendFunc(gl.ONE, gl.ONE)
		case SubtractiveBlend:
			gl.BlendEquation(gl.FUNC_REVERSE_SUBTRACT)
			gl.BlendFunc(gl.ONE, gl.ONE)
		case AlphaBlend:
			gl.BlendEquation(gl.FUNC_ADD)
			gl.BlendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA)
		}

		for _, t := range textures[blend] {
			if len(t.verts) == 0 {
				continue
			}
			gl.BindTexture(gl.TEXTURE_2D, t.tex)

			gl.BindVertexArray(t.vao)

			gl.BindBuffer(gl.ARRAY_BUFFER, t.vbo)
			gl.BufferData(gl.ARRAY_BUFFER, len(t.verts)*4, gl.Ptr(t.verts), gl.DYNAMIC_DRAW)

			var elements = ((len(t.verts) / 8) * 6) / 4

			if elements > len(indexData) {
				panic("TODO: grow index data")
			}

			gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer)
			gl.DrawElements(gl.TRIANGLES, int32(elements), gl.UNSIGNED_INT, gl.PtrOffset(0))

			t.verts = nil
		}
	}

	zIndex = 1.0

	window.GLSwap()

	CheckError()
}
