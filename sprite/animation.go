package sprite

import (
	"gitlab.com/thiderman/vanlig/image"
)

type Animation struct {
	Image      *image.Image
	StartFrame int32
	Frames     int32
	X          float32
	Y          float32
}

func NewAnimation(image *image.Image, frameCounter int32, frames int32, x float32, y float32) *Animation {
	anim := &Animation{
		Image:      image,
		StartFrame: frameCounter,
		Frames:     frames,
		X:          x,
		Y:          y,
	}

	return anim
}

func (a *Animation) Width() int32 {
	return a.Image.Width / a.Frames
}

func (a *Animation) Height() int32 {
	return a.Image.Height
}

func (a *Animation) currentFrame(frameCounter int32) int32 {
	return (frameCounter - a.StartFrame) % a.Frames
}

func (a *Animation) Finished(frameCounter int32) bool {
	return frameCounter != a.StartFrame && a.currentFrame(frameCounter) == 0
}

func (a *Animation) DrawFrame(frame int32) {
	a.Image.DrawSub(int32(a.X)-a.Width()/2, int32(a.Y)-a.Height()/2, frame*a.Width(), 0, a.Width(), a.Height())
}

func (a *Animation) Draw(frameCounter int32) {
	a.DrawFrame(a.currentFrame(frameCounter))
}
