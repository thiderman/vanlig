package sprite

import (
	"gitlab.com/thiderman/vanlig/image"
)

type Sprite struct {
	Image *image.Image
	X     float32
	Y     float32
}

func (s *Sprite) Draw() {
	s.Image.DrawAt(int32(s.X+0.5)-s.Image.Width/2, int32(s.Y+0.5)-s.Image.Height/2)
}
