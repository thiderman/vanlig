package game

import (
	"github.com/chewxy/math32"

	"gitlab.com/thiderman/vanlig/image"
	"gitlab.com/thiderman/vanlig/input"
	"gitlab.com/thiderman/vanlig/network"
	"gitlab.com/thiderman/vanlig/render"
	"gitlab.com/thiderman/vanlig/skills"
	"gitlab.com/thiderman/vanlig/sound"
	"gitlab.com/thiderman/vanlig/sprite"
	"gitlab.com/thiderman/vanlig/tilemap"
	"gitlab.com/thiderman/vanlig/units"

	"math/rand"
	"sort"
)

var blips []*units.Projectile
var enemies []*units.Unit
var players []*units.Player

var level *tilemap.TileMap

const acceleration float32 = 1.5

var shoot *sound.Sound
var explosion *sound.Sound
var hit *sound.Sound

var FrameCounter int32

func Init() {
	sound.PlayMusic()

	shoot = sound.NewSound("assets/snd/shoot.wav")
	explosion = sound.NewSound("assets/snd/explosion.wav")
	hit = sound.NewSound("assets/snd/hit.wav")

	projPlayer := image.NewImageBlend("assets/img/projectile_player.png", render.AlphaBlend)
	projEnemy := image.NewImageBlend("assets/img/projectile_enemy.png", render.AlphaBlend)
	explosionImg := image.NewImageBlend("assets/img/explosion.png", render.AdditiveBlend)

	playerSkills := skills.Skills{
		Main:   skills.NewSkill(projPlayer, 5, 3, 0, 6, 0.04),
		Second: skills.NewSkill(explosionImg, 50, 0, 1, 60, 0),
		Third:  skills.NewSkill(nil, 500, 1, 10, 1800, 0),
	}

	players = append(players, units.NewPlayer(image.NewImage("assets/img/player.png"), 0, 0, playerSkills, 10, 10, 1, 1))

	projBadguy := image.NewImageBlend("assets/img/projectile_badguy.png", render.AlphaBlend)

	badguySkills := skills.Skills{
		Main:   skills.NewSkill(projBadguy, 15, 10, 0, 6, 0),
		Second: skills.NewSkill(explosionImg, 150, 0, 1, 30, 0),
		Third:  skills.NewSkill(nil, 1500, 1, 10, 1800, 0),
	}

	players = append(players, units.NewPlayer(image.NewImage("assets/img/badguy.png"), 100, 0, badguySkills, 10, 10, 1, 1))

	level = tilemap.NewRandomMap(image.NewImage("assets/img/tiles.png"), 16)

	tilemap.CornerDecorator(level)
	tilemap.FloorDecorator(level)

	enemySkills := skills.Skills{
		Main:   skills.NewSkill(projEnemy, 1, 2, 0, 120, 0.1),
		Second: skills.NewSkill(nil, 50, 0, 1, 60, 0),
		Third:  skills.NewSkill(nil, 500, 0.5, 10, 1800, 0),
	}

	enemy1 := image.NewImage("assets/img/enemy1.png")
	enemy2 := image.NewImage("assets/img/enemy2.png")
	enemy3 := image.NewImage("assets/img/enemy3.png")
	enemy4 := image.NewImage("assets/img/enemy4.png")
	enemy5 := image.NewImage("assets/img/enemy5.png")
	enemy6 := image.NewImage("assets/img/enemy6.png")

	for i := 0; i <= 500; i++ {
		j := rand.Int31n(6)
		imageE := enemy1
		enemyHp := int32(10)
		if j == 1 {
			imageE = enemy2
			enemyHp = 50
		} else if j == 2 {
			imageE = enemy3
			enemyHp = 200
		} else if j == 3 {
			imageE = enemy4
			enemyHp = 300
		} else if j == 4 {
			imageE = enemy5
			enemyHp = 500
		} else if j == 5 {
			imageE = enemy6
			enemyHp = 1000
		}

		ex := float32(rand.Int31n(3200) - 1600)
		ey := float32(rand.Int31n(2400) - 1200)

		_, _, collision := level.GetCollision(ex, ey, 0, 0, tilemap.MOVEMENT)

		if !collision {
			enemies = append(enemies, units.NewUnit(imageE, ex, ey, enemySkills, enemyHp, 10, 1, 2))
		}
	}
}

func Update(p1 input.Inputs, p2 input.Inputs) {
	FrameCounter = FrameCounter + 1

	movement(p1, players[0])
	mouseInput(p1, players[0])

	movement(p2, players[1])
	mouseInput(p2, players[1])

	for _, enemy := range enemies {
		projectileRange := float32(100)
		if HasVisionOfPlayer(enemy, players[0]) && EnemyWithinRangeOfPlayer(enemy, players[0], projectileRange) {
			enemy.SetTarget(players[0].X, players[0].Y)

			if enemy.Skills.Main.Activate(FrameCounter) {
				ex := enemy.X
				ey := enemy.Y
				px := players[0].X
				py := players[0].Y
				direction := math32.Atan2(py-ey, px-ex)
				projectile := units.NewProjectile(
					sprite.NewAnimation(enemy.Skills.Main.Image, FrameCounter, 1, ex, ey),
					enemy.Skills.Main.Damage,
					enemy.Skills.Main.Speed,
					enemy.Skills.Main.Spread,
					direction,
					enemy.Team)
				shoot.Play()
				blips = append(blips, projectile)
			}
		} else {
			moveAI(enemy)
		}
	}

	if len(blips) > 10000 {
		blips = (blips)[len(blips)-10000:]
	}

	var delBlips []int
	var delEnemies []int

	for blipIndex, blip := range blips {
		xx := math32.Cos(blip.Direction) * blip.Speed
		yy := math32.Sin(blip.Direction) * blip.Speed
		mx, my, collision := level.GetCollision(blip.X, blip.Y, xx, yy, tilemap.ABSORB)
		if collision {
			delBlips = append([]int{blipIndex}, delBlips...)
		}
		blip.X += mx
		blip.Y += my
	}

	for blipIndex, blip := range blips {
		if blip.Animation.Frames > 1 && blip.Animation.Finished(FrameCounter) {
			delBlips = append([]int{blipIndex}, delBlips...)
			continue
		}

		bx := blip.X
		by := blip.Y
		for enemyIndex, enemy := range enemies {
			if blip.Team == enemy.Team {
				continue
			}
			ex := enemy.X
			ey := enemy.Y
			radius := float32(enemy.Image.Width / 2)
			if IsTargetHit(bx, by, ex, ey, radius) {
				enemy.HP -= blip.Damage
				if blip.Animation.Frames == 1 {
					delBlips = append([]int{blipIndex}, delBlips...)
				}

				if enemy.HP <= 0 {
					delEnemies = append([]int{enemyIndex}, delEnemies...)

					if len(delEnemies) == len(enemies) {
						println("You have won the game =)")
					}
				}

				hit.Play()
			}
		}

		if blip.Team == players[0].Team {
			continue
		}
		px := players[0].X
		py := players[0].Y
		radius := float32(players[0].Image.Width / 2)
		if IsTargetHit(bx, by, px, py, radius) {
			players[0].HP -= blip.Damage
			delBlips = append([]int{blipIndex}, delBlips...)
			if players[0].HP <= 0 {
				println("You have lost the game =(")
			}

			hit.Play()
		}
	}

	sort.Sort(sort.Reverse(sort.IntSlice(delBlips)))
	for _, index := range delBlips {
		if len(blips) > index {
			blips = append(blips[:index], blips[index+1:]...)
		}
	}
	sort.Sort(sort.Reverse(sort.IntSlice(delEnemies)))
	for _, index := range delEnemies {
		if len(enemies) > index {
			enemies = append(enemies[:index], enemies[index+1:]...)
		}
	}
}

func IsTargetHit(objectX float32, objectY float32, targetX float32, targetY float32, radius float32) bool {
	isHit := false
	xDiff := math32.Max(targetX, objectX) - math32.Min(targetX, objectX)
	if xDiff < radius {
		yDiff := math32.Max(targetY, objectY) - math32.Min(targetY, objectY)
		if yDiff < radius {
			isHit = true
		}
	}
	return isHit
}

func EnemyWithinRangeOfPlayer(unit *units.Unit, player *units.Player, xRange float32) bool {
	return WithinRange(unit.X, unit.Y, player.X, player.Y, xRange)
}

func WithinRange(x1, y1, x2, y2, xRange float32) bool {
	xDiff := math32.Max(x1, x2) - math32.Min(x1, x2)
	yDiff := math32.Max(y1, y2) - math32.Min(y1, y2)
	if xDiff < xRange && yDiff < xRange {
		return true
	}
	return false
}

func HasVisionOfPlayer(unit *units.Unit, player *units.Player) bool {
	if !WithinRange(unit.X, unit.Y, player.X, player.Y, float32(3000)) {
		return false
	}

	prevX := unit.X
	prevY := unit.Y
	dir := math32.Atan2(player.Y-unit.Y, player.X-unit.X)
	radius := float32(players[0].Image.Width / 2)

	loop := true
	i := 0
	collision := true
	for loop {
		i++
		if IsTargetHit(prevX, prevY, player.X, player.Y, radius) {
			//println("EXIT LOOP: HIT!")
			loop = false
		}
		if i > 1000 {
			println("ABORT LONG LOOP")
			loop = false
		}
		x := math32.Cos(dir) * float32(1)
		y := math32.Sin(dir) * float32(1)

		dx, dy, uCollision := level.GetCollision(prevX, prevY, x, y, tilemap.MOVEMENT)
		collision = uCollision
		prevX += dx
		prevY += dy
		if uCollision {
			//println("OUT OF SIGHT!")
			loop = false
		}

	}
	return !collision
}

func Draw() {
	render.SetCamera(int32(players[network.LocalPlayer].CameraX+0.5), int32(players[network.LocalPlayer].CameraY+0.5))

	level.Draw()

	for _, player := range players {
		player.Draw()
	}

	for _, enemy := range enemies {
		enemy.Draw()
	}

	for _, blip := range blips {
		blip.Draw(FrameCounter)
	}
}

func movement(inputs input.Inputs, player *units.Player) {
	var x float32
	var y float32

	if inputs.Up() {
		y -= acceleration
	}

	if inputs.Left() {
		x -= acceleration
	}

	if inputs.Down() {
		y += acceleration
	}

	if inputs.Right() {
		x += acceleration
	}
	mx, my, _ := level.GetCollision(player.X, player.Y, x, y, tilemap.MOVEMENT)

	player.X += mx
	player.Y += my
}

func moveAI(unit *units.Unit) {
	if !unit.TargetAquired {
		return
	}
	radius := float32(unit.Image.Width / 4)
	if IsTargetHit(unit.X, unit.Y, unit.TargetX, unit.TargetY, radius) {
		return
	}

	dir := math32.Atan2(unit.TargetY-unit.Y, unit.TargetX-unit.X)
	x := math32.Cos(dir) * unit.Speed
	y := math32.Sin(dir) * unit.Speed

	mx, my, _ := level.GetCollision(unit.X, unit.Y, x, y, tilemap.MOVEMENT)

	unit.X += mx * unit.Speed
	unit.Y += my * unit.Speed
}

func mouseInput(inputs input.Inputs, player *units.Player) {
	var projectile *units.Projectile

	px := player.X
	py := player.Y

	worldMouseX := player.CameraX + float32(inputs.MouseX)
	worldMouseY := player.CameraY + float32(inputs.MouseY)

	cx := (player.X*4 + worldMouseX) / 5
	cy := (player.Y*4 + worldMouseY) / 5

	player.CameraX = player.CameraX*0.92 + cx*0.08
	player.CameraY = player.CameraY*0.92 + cy*0.08

	aimDir := math32.Atan2(worldMouseY-py, worldMouseX-px)

	if inputs.Fire() && player.Skills.Main.Activate(FrameCounter) {

		projectile = units.NewProjectile(
			sprite.NewAnimation(player.Skills.Main.Image, FrameCounter, 1, player.X, player.Y),
			player.Skills.Main.Damage,
			player.Skills.Main.Speed,
			player.Skills.Main.Spread,
			aimDir,
			player.Team)

		shoot.Play()
	}

	if inputs.Power() && player.Skills.Second.Activate(FrameCounter) {
		projectile = units.NewProjectile(
			sprite.NewAnimation(player.Skills.Second.Image, FrameCounter, 12, worldMouseX, worldMouseY),
			player.Skills.Second.Damage,
			player.Skills.Second.Speed,
			player.Skills.Second.Spread,
			aimDir,
			player.Team)

		explosion.Play()
	}

	if projectile != nil {
		blips = append(blips, projectile)
	}
}
