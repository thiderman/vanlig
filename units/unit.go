package units

import (
	"gitlab.com/thiderman/vanlig/image"
	"gitlab.com/thiderman/vanlig/skills"
	"gitlab.com/thiderman/vanlig/sprite"
)

type Unit struct {
	sprite.Sprite
	HP            int32
	Mana          int32
	Speed         float32
	Skills        skills.Skills
	Team          int32
	TargetAquired bool
	TargetX       float32
	TargetY       float32
}

func NewUnit(image *image.Image, x float32, y float32, skills skills.Skills, hp int32, mana int32, speed float32, team int32) *Unit {
	unit := &Unit{
		Sprite: sprite.Sprite{
			Image: image,
			X:     x,
			Y:     y,
		},
		Skills: skills,
		HP:     hp,
		Mana:   mana,
		Speed:  speed,
		Team:   team,
	}

	return unit
}

func (u *Unit) SetTarget(x float32, y float32) {
	u.TargetAquired = true
	u.TargetX = x
	u.TargetY = y
}
