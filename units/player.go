package units

import (
	"gitlab.com/thiderman/vanlig/image"
	"gitlab.com/thiderman/vanlig/skills"
	"gitlab.com/thiderman/vanlig/sprite"
)

type Player struct {
	Unit
	CameraX float32
	CameraY float32
}

func NewPlayer(image *image.Image, x float32, y float32, skills skills.Skills, hp int32, mana int32, speed float32, team int32) *Player {
	player := &Player{
		Unit: Unit{
			Sprite: sprite.Sprite{
				Image: image,
				X:     x,
				Y:     y,
			},
			Skills: skills,
			HP:     hp,
			Mana:   mana,
			Speed:  speed,
			Team:   team,
		},
	}

	return player
}
