package units

import (
	"github.com/chewxy/math32"

	"gitlab.com/thiderman/vanlig/sprite"

	"math/rand"
)

type Projectile struct {
	sprite.Animation
	Damage    int32
	Speed     float32
	Direction float32
	Team      int32
}

func NewProjectile(anim *sprite.Animation, damage int32, speed float32, spread float32, direction float32, team int32) *Projectile {
	direction += spread * float32(rand.NormFloat64())

	anim.X += math32.Cos(direction) * speed * rand.Float32()
	anim.Y += math32.Sin(direction) * speed * rand.Float32()

	projectile := &Projectile{
		Animation: *anim,
		Damage:    damage,
		Speed:     speed,
		Direction: direction,
		Team:      team,
	}

	return projectile
}
