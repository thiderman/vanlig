#version 330 core

out vec4 FragColor;

in vec2 texCoord;
in vec3 color;

uniform sampler2D tex;

void main() {
  FragColor = texture(tex, texCoord);
  
  FragColor.rgb = FragColor.rgb * color;
  
  if(FragColor.a == 0.0) {
    discard;
  }
}
