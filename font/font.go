package font

import (
	"gitlab.com/thiderman/vanlig/render"

	"github.com/veandco/go-sdl2/sdl"
	"github.com/veandco/go-sdl2/ttf"
)

type Font int

const (
	MainMenuFont Font = iota
	SettingsFont
	OverlayFont
	NumFonts
)

var fonts [NumFonts]*ttf.Font

type Text struct {
	texture *render.Texture
	Width   int32
	Height  int32
}

type HorizAlign int

const (
	AlignLeft HorizAlign = iota
	AlignCenter
	AlignRight
)

type VertAlign int

const (
	AlignTop VertAlign = iota
	AlignMiddle
	AlignBottom
)

func Init() {
	var err error

	err = ttf.Init()

	if err != nil {
		panic(err)
	}

	fonts[MainMenuFont], err = ttf.OpenFont("assets/font/prstart.ttf", 16)

	if err != nil {
		panic(err)
	}
}

func RenderText(font Font, text string) *Text {
	width, height, err := fonts[font].SizeUTF8(text)

	if err != nil {
		panic(err)
	}

	surf, err := fonts[font].RenderUTF8Blended(text, sdl.Color{255, 255, 255, 255})

	if err != nil {
		panic(err)
	}

	texture := render.LoadTexture(surf, render.AlphaBlend)

	surf.Free()

	return &Text{
		texture: texture,
		Width:   int32(width),
		Height:  int32(height),
	}
}

func screenX(x int32) float32 {
	return float32(x) / 160.0
}

func screenY(y int32) float32 {
	return float32(y) / 120.0
}

func (t *Text) Draw(x int32, y int32, color sdl.Color) {
	t.DrawAligned(x, y, color, AlignCenter)
}

func (t *Text) DrawAligned(x int32, y int32, color sdl.Color, align HorizAlign) {
	t.DrawVertAligned(x, y, color, align, AlignMiddle)
}

func (t *Text) DrawVertAligned(x int32, y int32, color sdl.Color, align HorizAlign, vertalign VertAlign) {
	switch align {
	case AlignCenter:
		x -= t.Width / 2
	case AlignRight:
		x -= t.Width
	}

	switch vertalign {
	case AlignMiddle:
		y -= t.Height / 2
	case AlignBottom:
		y -= t.Height
	}

	t.texture.DrawSimpleQuadColor(screenX(x), screenY(y), screenX(x+t.Width), screenY(y+t.Height), color)
}
