package network

import (
	"bytes"
	"encoding/binary"
	"log"
	"math/rand"
	"net"

	"gitlab.com/thiderman/vanlig/input"
)

type Packet struct {
	Frame uint32

	Player1 input.Inputs
	Player2 input.Inputs
}

type Match struct {
	Addr uint32
	Port uint16
}

const DELAY uint32 = 2

var socket *net.UDPConn

var matchBackoff int

var localAddr *net.UDPAddr

var matchAddr *net.UDPAddr
var matchInChan = make(chan Match, 10)
var matchOutChan = make(chan Match, 10)

var frameCounter uint32

var p2pAddr *net.UDPAddr
var p2pInChan = make(chan Packet, 10)
var p2pOutChan = make(chan Packet, 10)

var lastPacket *Packet = &Packet{Frame: 0, Player1: input.Inputs{}, Player2: input.Inputs{}}

var LocalPlayer uint32

func Init() {
	var err error

	frameCounter = 0

	socket, err = net.ListenUDP("udp4", nil)
	if err != nil {
		log.Fatalf("ListenUDP: %s", err)
	}

	localAddr = socket.LocalAddr().(*net.UDPAddr)

	matchAddr, err = net.ResolveUDPAddr("udp", "vanlig.aali.se:32123")
	if err != nil {
		log.Fatalf("matchAddr: %s", err)
	}

	println("Match addr: ", matchAddr.String())

	go func() {
		for {

			buffer := make([]byte, 256)

			_, addr, err := socket.ReadFromUDP(buffer)

			if err != nil {
				log.Fatalf("ReadFromUDP: %s", err)
			}

			if addr.IP.Equal(matchAddr.IP) && addr.Port == matchAddr.Port {
				match := Match{}
				err = binary.Read(bytes.NewReader(buffer), binary.LittleEndian, &match)
				if err != nil {
					log.Printf("read match: %s", err)
					continue
				}
				matchInChan <- match
			} else {
				packet := Packet{}
				err = binary.Read(bytes.NewReader(buffer), binary.LittleEndian, &packet)
				if err != nil {
					log.Printf("read packet: %s", err)
					continue
				}
				p2pInChan <- packet
			}
		}
	}()

	go func() {
		for {
			match := <-matchOutChan

			var buf bytes.Buffer
			err := binary.Write(&buf, binary.LittleEndian, &match)
			if err != nil {
				log.Fatalf("write match: %s", err)
			}

			_, err = socket.WriteToUDP(buf.Bytes(), matchAddr)

			if err != nil {
				log.Fatalf("WriteToUDP match: %s", err)
			}
		}
	}()

	go func() {
		for {
			packet := <-p2pOutChan

			var buf bytes.Buffer
			err := binary.Write(&buf, binary.LittleEndian, &packet)
			if err != nil {
				log.Fatalf("write packet: %s", err)
			}

			_, err = socket.WriteToUDP(buf.Bytes(), p2pAddr)

			if err != nil {
				log.Fatalf("WriteToUDP packet: %s", err)
			}
		}
	}()
}

func isServer() bool {
	return localAddr.Port > p2pAddr.Port
}

var frameCounterRepeat uint32

func NextFrame(player input.Inputs) (input.Inputs, input.Inputs, bool) {
	if frameCounter < 5 {
		more := true
		for more {
			select {
			case match := <-matchInChan:
				if match.Port != 0 {
					ip := make([]byte, 4)
					binary.BigEndian.PutUint32(ip, match.Addr)
					p2pAddr = &net.UDPAddr{IP: ip, Port: int(match.Port)}

					println("Receive matchmaking ", p2pAddr.String())
				}
			default:
				more = false
			}
		}

		if matchBackoff > 0 {
			matchBackoff--
		} else {
			match := Match{Addr: 0, Port: 0}

			matchOutChan <- match

			matchBackoff = 30 + rand.Intn(30)
		}
	}

	if p2pAddr != nil {
		if !isServer() {
			LocalPlayer = 1

			if frameCounter == 0 {
				packet := Packet{Frame: 0, Player2: player}
				p2pOutChan <- packet

				for i := uint32(1); i < DELAY; i++ {
					packet = Packet{Frame: i, Player2: player}
					p2pOutChan <- packet
				}
			}

			packet := Packet{Frame: frameCounter + DELAY, Player2: player}
			p2pOutChan <- packet

			more := true
			for more {
				select {
				case packet := <-p2pInChan:
					if packet.Frame == frameCounter {
						frameCounter++
						return packet.Player1, packet.Player2, true
					}
				default:
					more = false
				}
			}
		} else {
			LocalPlayer = 0

			more := true
			for more {
				select {
				case packet := <-p2pInChan:
					if packet.Frame == frameCounter {
						newPacket := Packet{Frame: frameCounter, Player1: player, Player2: packet.Player2}
						p2pOutChan <- newPacket
						lastPacket = &newPacket
						frameCounter++
						return player, packet.Player2, true
					}
				default:
					more = false
				}
			}

			p2pOutChan <- *lastPacket
		}
	} else {
		more := true
		for more {
			select {
			case _ = <-p2pInChan:

			default:
				more = false
			}
		}
	}

	if frameCounter > 0 {
		if frameCounterRepeat != frameCounter {
			println("lag frame(s) at ", frameCounter)
		}
	}

	frameCounterRepeat = frameCounter

	return input.Inputs{}, input.Inputs{}, false
}
