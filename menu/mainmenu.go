package menu

import (
	"gitlab.com/thiderman/vanlig/font"
	"gitlab.com/thiderman/vanlig/global"

	"os"
)

type MainMenu struct {
	title    *font.Text
	single   MenuItem
	multi    MenuItem
	settings MenuItem
	quit     MenuItem
}

var mainMenu MainMenu

func InitMainMenu() *MainMenu {
	mainMenu.title = font.RenderText(font.MainMenuFont, "3 1 2 Panic!")
	mainMenu.single.text = font.RenderText(font.MainMenuFont, "Single Player")
	mainMenu.multi.text = font.RenderText(font.MainMenuFont, "Multi Player")
	mainMenu.settings.text = font.RenderText(font.MainMenuFont, "Settings")
	mainMenu.quit.text = font.RenderText(font.MainMenuFont, "Quit")

	return &mainMenu
}

func (m *MainMenu) Update() (*font.Text, []*MenuItem) {
	return m.title, []*MenuItem{&m.single, &m.multi, &m.settings, &m.quit}
}

func (m *MainMenu) Activate(i *MenuItem) {
	if i == &mainMenu.single {
		global.Multiplayer = false
		CloseMenu()
	}

	if i == &mainMenu.multi {
		global.Multiplayer = true
		CloseMenu()
	}

	if i == &mainMenu.settings {

	}

	if i == &mainMenu.quit {
		os.Exit(0)
	}
}

func (m *MainMenu) Exit() {

}
