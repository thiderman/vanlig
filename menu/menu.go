package menu

import (
	"gitlab.com/thiderman/vanlig/font"
	"gitlab.com/thiderman/vanlig/input"
	"gitlab.com/thiderman/vanlig/render"

	"github.com/veandco/go-sdl2/sdl"
)

type MenuItem struct {
	text *font.Text
}

type MenuController interface {
	Update() (*font.Text, []*MenuItem)
	Activate(*MenuItem)
	Exit()
}

const yPad int32 = 20
const yOffset int32 = -40

var currentMenu MenuController
var title *font.Text
var menuItems []*MenuItem
var selectedItem int
var prevInput *input.Inputs

func OpenMenu(menu MenuController) {
	if currentMenu != nil {
		currentMenu.Exit()
	}

	title = nil
	menuItems = nil
	selectedItem = -1
	prevInput = nil

	currentMenu = menu
}

func CloseMenu() {
	OpenMenu(nil)
}

func itemTop(index int) int32 {
	height := menuItems[0].text.Height

	return yOffset + int32(index)*(height+yPad)
}

func Update(input input.Inputs) bool {
	if currentMenu != nil {
		title, menuItems = currentMenu.Update()

		repeat := input.Repeat(prevInput)

		if repeat.Up() {
			selectedItem--

			if selectedItem < 0 {
				selectedItem = 0
			}
		}

		if repeat.Down() {
			selectedItem++

			if selectedItem >= len(menuItems) {
				selectedItem = len(menuItems) - 1
			}
		}

		if prevInput != nil {
			if input.MouseX != prevInput.MouseX || input.MouseY != prevInput.MouseY {
				selectedItem = -1

				for index, item := range menuItems {
					if int32(input.MouseX) >= -item.text.Width/2 && int32(input.MouseX) < item.text.Width/2 {
						if int32(input.MouseY) >= itemTop(index) && int32(input.MouseY) < itemTop(index)+item.text.Height {
							selectedItem = index
						}
					}
				}
			}
		}

		if repeat.Fire() && selectedItem >= 0 && selectedItem < len(menuItems) {
			currentMenu.Activate(menuItems[selectedItem])
		}

		prevInput = &input

		return true
	}

	return false
}

func Draw() {
	if currentMenu != nil {
		render.SetCamera(0, 0)

		title.DrawVertAligned(0, itemTop(-1), sdl.Color{32, 32, 32, 255}, font.AlignCenter, font.AlignTop)

		for index, item := range menuItems {
			if index == selectedItem {
				item.text.DrawVertAligned(0, itemTop(index), sdl.Color{180, 180, 180, 255}, font.AlignCenter, font.AlignTop)
			} else {
				item.text.DrawVertAligned(0, itemTop(index), sdl.Color{128, 128, 128, 255}, font.AlignCenter, font.AlignTop)
			}
		}
	}
}
