module gitlab.com/thiderman/vanlig

go 1.15

require (
	github.com/chewxy/math32 v1.0.8
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7
	github.com/veandco/go-sdl2 v0.4.18
	gopkg.in/yaml.v2 v2.4.0
)
