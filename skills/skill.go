package skills

import (
	"gitlab.com/thiderman/vanlig/image"
)

type Skills struct {
	Main   Skill
	Second Skill
	Third  Skill
}

type Skill struct {
	Image  *image.Image
	Damage int32
	Speed  float32
	Cost   int32
	Spread float32

	cooldown      float32
	cooldownFrame int32
}

func NewSkill(image *image.Image, damage int32, speed float32, cost int32, cooldown float32, spread float32) Skill {
	skill := Skill{
		Image:  image,
		Damage: damage,
		Speed:  speed,
		Cost:   cost,
		Spread: spread,

		cooldown: cooldown,
	}
	return skill
}

func (s *Skill) Activate(frame int32) bool {
	if s.cooldownFrame < frame {
		s.cooldownFrame = frame + int32(s.cooldown)
		return true
	}

	return false
}
