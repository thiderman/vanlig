package input

import (
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/thiderman/vanlig/settings"
)

type Button uint32

const (
	Up Button = 1 << iota
	Left
	Down
	Right
	Fire
	Power
	Use
	Swap
)

type Inputs struct {
	Buttons uint32
	MouseX  int16
	MouseY  int16
}

func (i *Inputs) Up() bool {
	return (i.Buttons & uint32(Up)) != 0
}

func (i *Inputs) Left() bool {
	return (i.Buttons & uint32(Left)) != 0
}

func (i *Inputs) Down() bool {
	return (i.Buttons & uint32(Down)) != 0
}

func (i *Inputs) Right() bool {
	return (i.Buttons & uint32(Right)) != 0
}

func (i *Inputs) Fire() bool {
	return (i.Buttons & uint32(Fire)) != 0
}

func (i *Inputs) Power() bool {
	return (i.Buttons & uint32(Power)) != 0
}

func (i *Inputs) Use() bool {
	return (i.Buttons & uint32(Use)) != 0
}

func (i *Inputs) Swap() bool {
	return (i.Buttons & uint32(Swap)) != 0
}

func (i *Inputs) pressButton(button Button) {
	i.Buttons |= uint32(button)
}

func (i *Inputs) releaseButton(button Button) {
	i.Buttons &^= uint32(button)
}

func (i *Inputs) Repeat(prev *Inputs) *Inputs {
	if prev == nil {
		return i
	}

	return &Inputs{
		Buttons: i.Buttons &^ prev.Buttons,
		MouseX:  i.MouseX,
		MouseY:  i.MouseY,
	}
}

var inputs Inputs

func Init() {
	sdl.JoystickEventState(sdl.ENABLE)
	sdl.GameControllerEventState(sdl.ENABLE)
}

func HandleInputEvent(event sdl.Event) bool {
	switch t := event.(type) {
	case *sdl.KeyboardEvent:
		if t.Repeat == 0 {
			if t.State == sdl.PRESSED {
				switch t.Keysym.Scancode {
				case settings.Global.Keyboard.Up:
					inputs.pressButton(Up)
				case settings.Global.Keyboard.Left:
					inputs.pressButton(Left)
				case settings.Global.Keyboard.Down:
					inputs.pressButton(Down)
				case settings.Global.Keyboard.Right:
					inputs.pressButton(Right)
				case settings.Global.Keyboard.Fire:
					inputs.pressButton(Fire)
				case settings.Global.Keyboard.Power:
					inputs.pressButton(Power)
				case settings.Global.Keyboard.Use:
					inputs.pressButton(Use)
				case settings.Global.Keyboard.Up:
					inputs.pressButton(Up)
				}
			} else {
				switch t.Keysym.Scancode {
				case settings.Global.Keyboard.Up:
					inputs.releaseButton(Up)
				case settings.Global.Keyboard.Left:
					inputs.releaseButton(Left)
				case settings.Global.Keyboard.Down:
					inputs.releaseButton(Down)
				case settings.Global.Keyboard.Right:
					inputs.releaseButton(Right)
				case settings.Global.Keyboard.Fire:
					inputs.releaseButton(Fire)
				case settings.Global.Keyboard.Power:
					inputs.releaseButton(Power)
				case settings.Global.Keyboard.Use:
					inputs.releaseButton(Use)
				case settings.Global.Keyboard.Swap:
					inputs.releaseButton(Swap)
				}
			}
		}
		return true

	case *sdl.ControllerDeviceEvent:
		switch t.Type {
		case sdl.CONTROLLERDEVICEADDED:
			sdl.GameControllerOpen(int(t.Which))
		}

	case *sdl.ControllerButtonEvent:
		if t.State == sdl.PRESSED {
			switch t.Button {
			case settings.Global.Gamepad.Up:
				inputs.pressButton(Up)
			case settings.Global.Gamepad.Left:
				inputs.pressButton(Left)
			case settings.Global.Gamepad.Down:
				inputs.pressButton(Down)
			case settings.Global.Gamepad.Right:
				inputs.pressButton(Right)
			case settings.Global.Gamepad.Fire:
				inputs.pressButton(Fire)
			case settings.Global.Gamepad.Power:
				inputs.pressButton(Power)
			case settings.Global.Gamepad.Use:
				inputs.pressButton(Use)
			case settings.Global.Gamepad.Swap:
				inputs.pressButton(Swap)
			}
		} else {
			switch t.Button {
			case settings.Global.Gamepad.Up:
				inputs.releaseButton(Up)
			case settings.Global.Gamepad.Left:
				inputs.releaseButton(Left)
			case settings.Global.Gamepad.Down:
				inputs.releaseButton(Down)
			case settings.Global.Gamepad.Right:
				inputs.releaseButton(Right)
			case settings.Global.Gamepad.Fire:
				inputs.releaseButton(Fire)
			case settings.Global.Gamepad.Power:
				inputs.releaseButton(Power)
			case settings.Global.Gamepad.Use:
				inputs.releaseButton(Use)
			case settings.Global.Gamepad.Swap:
				inputs.releaseButton(Swap)
			}
		}
		return true

	case *sdl.ControllerAxisEvent:
		switch t.Axis {
		case settings.Global.Gamepad.HAxis:
			inputs.MouseX = int16((int32(t.Value) * 120) / 32768)
		case settings.Global.Gamepad.VAxis:
			inputs.MouseY = int16((int32(t.Value) * 120) / 32768)
		}
		return true

	case *sdl.MouseButtonEvent:
		if t.State == sdl.PRESSED {
			switch t.Button {
			case settings.Global.Mouse.Fire:
				inputs.pressButton(Fire)
			case settings.Global.Mouse.Power:
				inputs.pressButton(Power)
			case settings.Global.Mouse.Use:
				inputs.pressButton(Use)
			case settings.Global.Mouse.Swap:
				inputs.pressButton(Swap)
			}
		} else {
			switch t.Button {
			case settings.Global.Mouse.Fire:
				inputs.releaseButton(Fire)
			case settings.Global.Mouse.Power:
				inputs.releaseButton(Power)
			case settings.Global.Mouse.Use:
				inputs.releaseButton(Use)
			case settings.Global.Mouse.Swap:
				inputs.releaseButton(Swap)
			}
		}
		return true

	case *sdl.MouseMotionEvent:
		inputs.MouseX = int16(((t.X - 512) * 320) / 1024)
		inputs.MouseY = int16(((t.Y - 384) * 240) / 768)

		return true
	}

	return false
}

func ReadInputs() Inputs {
	return inputs
}
