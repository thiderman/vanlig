package sound

import (
	"github.com/veandco/go-sdl2/mix"

	"fmt"
)

var SoundEnabled bool

func Init() {
	err := mix.Init(mix.INIT_MP3)
	if err != nil {
		panic(err)
	}

	err = mix.OpenAudio(22050, mix.DEFAULT_FORMAT, 2, 4096)
	if err != nil {
		fmt.Println(err)
		return
	}

	mix.AllocateChannels(256)
	mix.Volume(-1, 32)
	mix.VolumeMusic(64)

	SoundEnabled = true
}

func PlayMusic() {
	if SoundEnabled {
		music, err := mix.LoadMUS("assets/snd/test.mp3")
		if err != nil {
			panic(err)
		}

		err = music.Play(-1)
		if err != nil {
			panic(err)
		}
	}
}

type Sound struct {
	chunk *mix.Chunk
}

func NewSound(path string) *Sound {
	if SoundEnabled {
		chunk, err := mix.LoadWAV(path)
		if err != nil {
			panic(err)
		}

		s := &Sound{
			chunk: chunk,
		}

		return s
	}

	return nil
}

func (s *Sound) Play() {
	if SoundEnabled {
		_, err := s.chunk.Play(-1, 0)
		if err != nil {
			panic(err)
		}
	}
}
