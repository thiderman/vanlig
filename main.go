package main

import (
	"github.com/veandco/go-sdl2/sdl"

	"gitlab.com/thiderman/vanlig/font"
	"gitlab.com/thiderman/vanlig/game"
	"gitlab.com/thiderman/vanlig/global"
	"gitlab.com/thiderman/vanlig/input"
	"gitlab.com/thiderman/vanlig/menu"
	"gitlab.com/thiderman/vanlig/network"
	"gitlab.com/thiderman/vanlig/render"
	"gitlab.com/thiderman/vanlig/settings"
	"gitlab.com/thiderman/vanlig/sound"

	"log"
	"runtime"
	"time"
)

func main() {
	println("Start")

	runtime.LockOSThread()

	render.Init(1024, 768)

	font.Init()

	input.Init()

	network.Init()

	sound.Init()

	println("Loading settings file")
	settings.LoadSettings("./game.yaml")
	log.Printf("%+v", settings.Global)
	err := settings.SaveSettings("./game.yaml")
	if err != nil {
		log.Fatal("Oh noes")
	}

	game.Init()

	mainMenu := menu.InitMainMenu()

	menu.OpenMenu(mainMenu)

	println("Main loop start")

	frameTicker := time.NewTicker(time.Second / 60)

	for true {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.KeyboardEvent:
				if t.State == sdl.PRESSED && t.Keysym.Scancode == sdl.SCANCODE_ESCAPE {
					menu.OpenMenu(mainMenu)
				}
			case *sdl.QuitEvent:
				println("Quit")
				return
			}

			if input.HandleInputEvent(event) {
				continue
			}
		}

		render.Clear()

		localInputs := input.ReadInputs()

		if menu.Update(localInputs) {
			game.Draw()
			menu.Draw()
		} else {
			if global.Multiplayer {
				p1, p2, valid := network.NextFrame(localInputs)

				if valid {
					game.Update(p1, p2)
				}
			} else {
				game.Update(localInputs, input.Inputs{})
			}

			game.Draw()
		}

		render.Swap()

		<-frameTicker.C
	}
}
