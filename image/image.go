package image

import (
	"github.com/veandco/go-sdl2/img"
	"github.com/veandco/go-sdl2/sdl"
	"gitlab.com/thiderman/vanlig/render"
)

func init() {
	_ = img.Init(img.INIT_PNG)
}

type Image struct {
	path    string
	texture *render.Texture

	Width  int32
	Height int32
}

func NewImage(path string) *Image {
	return NewImageBlend(path, render.NoBlend)
}

func NewImageBlend(path string, blend render.BlendMode) *Image {
	var surface *sdl.Surface
	var err error

	i := &Image{
		path: path,
	}

	surface, err = img.Load(i.path)
	if err != nil {
		panic(err)
	}

	i.Width = surface.W
	i.Height = surface.H

	i.texture = render.LoadTexture(surface, blend)

	surface.Free()

	return i
}

func (i *Image) screenX(x int32) float32 {
	return float32(x) / 160.0
}

func (i *Image) screenY(y int32) float32 {
	return float32(y) / 120.0
}

func (i *Image) texU(x int32) float32 {
	return float32(x) / float32(i.Width)
}

func (i *Image) texV(y int32) float32 {
	return float32(y) / float32(i.Height)
}

func (i *Image) DrawAt(x int32, y int32) {
	i.texture.DrawSimpleQuad(i.screenX(x), i.screenY(y), i.screenX(x+i.Width), i.screenY(y+i.Height))
}

func (i *Image) DrawSub(x int32, y int32, srcX int32, srcY int32, w int32, h int32) {
	i.texture.DrawQuad(i.screenX(x), i.screenY(y), i.screenX(x+w), i.screenY(y+h), i.texU(srcX), i.texV(srcY), i.texU(srcX+w), i.texV(srcY+h))
}
