package tilemap

import (
	"gitlab.com/thiderman/vanlig/image"

	"fmt"
	"math"
	"math/rand"
)

type Path struct {
	left    *Path
	right   *Path
	forward *Path
	length  int32
	width   int32
}

func normNatural(stddev int32) int32 {
	return int32(math.Abs(rand.NormFloat64() * float64(stddev) / 2))
}

const widthStdDev int32 = 5
const lengthStdDev int32 = 20

const rollThreshold int32 = 20

func rollFavorableEarly(score int32) bool {
	return normNatural(score) > rollThreshold
}

func rollFavorableLate(score int32) bool {
	return !rollFavorableEarly(score)
}

func randomPathTree(score int32) *Path {
	if score <= 10 {
		return nil
	}

	p := &Path{
		length: normNatural(lengthStdDev) + 1,
		width:  normNatural(widthStdDev) + 1,
	}

	remainingScore := score - p.length*p.width

	if remainingScore > 0 {
		if rollFavorableEarly(remainingScore) {
			leftScore := normNatural(remainingScore)

			p.left = randomPathTree(leftScore)

			remainingScore = remainingScore - leftScore
		}
	}

	if remainingScore > 0 {
		if rollFavorableEarly(remainingScore) {
			rightScore := normNatural(remainingScore)

			p.right = randomPathTree(rightScore)

			remainingScore = remainingScore - rightScore
		}
	}

	if remainingScore > 0 {
		p.forward = randomPathTree(remainingScore)
	}

	return p
}

func scoreTree(tree *Path) int32 {
	if tree == nil {
		return 0
	}

	return tree.length*tree.width + scoreTree(tree.left) + scoreTree(tree.right) + scoreTree(tree.forward)
}

func printTree(tree *Path, prefix string) {
	if tree != nil {
		fmt.Println(prefix, tree.length, tree.width)

		printTree(tree.left, prefix+"<")
		printTree(tree.right, prefix+">")
		printTree(tree.forward, prefix+"^")
	}
}

type Direction int

const (
	North Direction = iota
	West
	South
	East
)

func startXY(x int32, y int32, dir Direction, tree *Path) (int32, int32) {
	startX := x - tree.width/2
	startY := y - tree.width/2

	if dir == North {
		startY = y - tree.length + 1
	}

	if dir == West {
		startX = x - tree.length + 1
	}

	if dir == South {
		startY = y
	}

	if dir == East {
		startX = x
	}

	return startX, startY
}

func endXY(x int32, y int32, dir Direction, tree *Path) (int32, int32) {
	endX := x + (tree.width+1)/2
	endY := y + (tree.width+1)/2

	if dir == North {
		endY = y + 1
	}

	if dir == West {
		endX = x + 1
	}

	if dir == South {
		endY = y + tree.length
	}

	if dir == East {
		endX = x + tree.length
	}

	return endX, endY
}

func placeTiles(t *TileMap, x int32, y int32, dir Direction, tree *Path) int32 {
	var tilesPlaced int32

	if tree != nil {
		x1, y1 := startXY(x, y, dir, tree)
		x2, y2 := endXY(x, y, dir, tree)

		for xx := x1; xx < x2; xx++ {
			for yy := y1; yy < y2; yy++ {
				if t.tiles[xx][yy] != 0 {
					tilesPlaced++
				}

				t.tiles[xx][yy] = 0
			}
		}

		offset := tree.length - 1

		if dir == North {
			tilesPlaced += placeTiles(t, x, y-offset, West, tree.left)
			tilesPlaced += placeTiles(t, x, y-offset, East, tree.right)
			tilesPlaced += placeTiles(t, x, y-offset, North, tree.forward)
		}

		if dir == West {
			tilesPlaced += placeTiles(t, x-offset, y, South, tree.left)
			tilesPlaced += placeTiles(t, x-offset, y, North, tree.right)
			tilesPlaced += placeTiles(t, x-offset, y, West, tree.forward)
		}

		if dir == South {
			tilesPlaced += placeTiles(t, x, y+offset, East, tree.left)
			tilesPlaced += placeTiles(t, x, y+offset, West, tree.right)
			tilesPlaced += placeTiles(t, x, y+offset, South, tree.forward)
		}

		if dir == East {
			tilesPlaced += placeTiles(t, x+offset, y, North, tree.left)
			tilesPlaced += placeTiles(t, x+offset, y, South, tree.right)
			tilesPlaced += placeTiles(t, x+offset, y, East, tree.forward)
		}
	}

	return tilesPlaced
}

func NewRandomMap(image *image.Image, tileSize int32) *TileMap {
	t := &TileMap{
		image:    image,
		tileSize: tileSize,
	}

	var x int32
	var y int32

	for x = 0; x < MAP_WIDTH; x++ {
		for y = 0; y < MAP_HEIGHT; y++ {
			t.tiles[x][y] = 1
		}
	}

	pathTree := randomPathTree(1000)

	tilesPlaced := placeTiles(t, MAP_WIDTH/2-pathTree.length/2, MAP_HEIGHT/2, East, pathTree)

	fmt.Println("Final map score:", scoreTree(pathTree), "tiles placed:", tilesPlaced)

	return t
}
