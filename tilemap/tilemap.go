package tilemap

import (
	"bufio"
	"gitlab.com/thiderman/vanlig/image"
	"math/rand"
	"os"
)

type CollisionType int32

const (
	MOVEMENT = iota
	ABSORB
	BOUNCE
)
const MAP_WIDTH = 200
const MAP_HEIGHT = 150

type TileMap struct {
	image    *image.Image
	tileSize int32
	tiles    [MAP_WIDTH][MAP_HEIGHT]int32
}

func NewLevelMap(image *image.Image, tileSize int32) *TileMap {
	t := &TileMap{
		image:    image,
		tileSize: tileSize,
	}
	var tile int32

	file, err := os.Open("tilemap/level_1")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	for i, row := range lines {
		for j, col := range row {
			switch col {
			case '#':
				tile = 25
			case ' ':
				tile = rand.Int31n(8)
				if tile < 4 {
					tile = 0
				}
			case '%':
				tile = 1
			case '1':
				tile = 8
			case '2':
				tile = 9
			case '3':
				tile = 10
			case '4':
				tile = 11
			case '-':
				tile = 0
			}
			t.tiles[j][i] = tile
		}
	}

	return t
}

func contains(needle int32, haystack []int32) bool {
	for _, i := range haystack {
		if i == needle {
			return true
		}
	}
	return false
}

// TODO: Hantera när dx och dy är > 16 eller < -16
func (t *TileMap) GetCollision(x float32, y float32, dx float32, dy float32, collisionType CollisionType) (float32, float32, bool) {
	nonCollisionTiles := []int32{0, 4, 5, 6, 7, 8, 9, 10, 11, 24, 25, 26, 27}
	xCollision := true
	yCollision := true
	xd := float32(0)
	if contains(t.GetTile(x+dx, y), nonCollisionTiles) {
		xd = dx
		xCollision = false
	}
	yd := float32(0)
	if contains(t.GetTile(x, y+dy), nonCollisionTiles) {
		yd = dy
		yCollision = false
	}
	if collisionType == ABSORB && (xCollision || yCollision) {
		xd = 0
		yd = 0
	}
	return xd, yd, xCollision || yCollision
}

func (t *TileMap) GetTile(x float32, y float32) int32 {
	xt := int32(x+MAP_WIDTH*16/2) / 16
	yt := int32(y+MAP_HEIGHT*16/2) / 16
	return t.tiles[xt][yt]
}

func (t *TileMap) TileX(tile int32) int32 {
	return (tile * t.tileSize) % t.image.Width
}

func (t *TileMap) TileY(tile int32) int32 {
	return ((tile * t.tileSize) / t.image.Width) * t.tileSize
}

func (t *TileMap) DrawTile(x int32, y int32) {
	t.image.DrawSub(t.tileSize*(x-MAP_WIDTH/2), t.tileSize*(y-MAP_HEIGHT/2), t.TileX(t.tiles[x][y]), t.TileY(t.tiles[x][y]), t.tileSize, t.tileSize)
}

func (t *TileMap) Draw() {
	var x int32
	var y int32

	for x = 0; x < MAP_WIDTH; x++ {
		for y = 0; y < MAP_HEIGHT; y++ {
			t.DrawTile(x, y)
		}
	}
}
