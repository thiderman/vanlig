package tilemap

import (
	"math/rand"
)

type decoratorPattern struct {
	oldPattern [3][3]int32
	newTile    int32
}

var cornerPatterns = []decoratorPattern{
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 1, 1}, {0, 0, 1}, {-1, 0, -1}},
		newTile:    8,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 0, -1}, {0, 0, 1}, {-1, 1, 1}},
		newTile:    9,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 0, -1}, {1, 0, 0}, {1, 1, -1}},
		newTile:    10,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{1, 1, -1}, {1, 0, 0}, {-1, 0, -1}},
		newTile:    11,
	},

	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 0, -1}, {0, 1, 1}, {-1, 1, 1}},
		newTile:    20,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 0, -1}, {1, 1, 0}, {1, 1, -1}},
		newTile:    21,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{1, 1, -1}, {1, 1, 0}, {-1, 0, -1}},
		newTile:    22,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 1, 1}, {0, 1, 1}, {-1, 0, -1}},
		newTile:    23,
	},

	decoratorPattern{
		oldPattern: [3][3]int32{{1, 1, 1}, {1, 0, 1}, {-1, -1, -1}},
		newTile:    24,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 1, 1}, {-1, 0, 1}, {-1, 1, 1}},
		newTile:    25,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, -1, -1}, {1, 0, 1}, {1, 1, 1}},
		newTile:    26,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{1, 1, -1}, {1, 0, -1}, {1, 1, -1}},
		newTile:    27,
	},

	decoratorPattern{
		oldPattern: [3][3]int32{{0, 0, 0}, {0, 1, 0}, {-1, -1, -1}},
		newTile:    28,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, 0, 0}, {-1, 1, 0}, {-1, 0, 0}},
		newTile:    29,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{-1, -1, -1}, {0, 1, 0}, {0, 0, 0}},
		newTile:    30,
	},
	decoratorPattern{
		oldPattern: [3][3]int32{{0, 0, -1}, {0, 1, -1}, {0, 0, -1}},
		newTile:    31,
	},
}

func CornerDecorator(t *TileMap) {
	oldTiles := t.tiles

	var x, y, i, j int32

	for x = 1; x < MAP_WIDTH-1; x++ {
		for y = 1; y < MAP_HEIGHT-1; y++ {
			for p := 0; p < len(cornerPatterns); p++ {
				match := true

				for i = 0; i < 3; i++ {
					for j = 0; j < 3; j++ {
						if cornerPatterns[p].oldPattern[j][i] != -1 && cornerPatterns[p].oldPattern[j][i] != oldTiles[x+i-1][y+j-1] {
							match = false
							break
						}
					}

					if !match {
						break
					}
				}

				if match {
					t.tiles[x][y] = cornerPatterns[p].newTile
				}
			}
		}
	}
}

func FloorDecorator(t *TileMap) {
	oldTiles := t.tiles

	var x, y int32

	for x = 1; x < MAP_WIDTH-1; x++ {
		for y = 1; y < MAP_HEIGHT-1; y++ {
			if oldTiles[x][y] == 0 {
				t.tiles[x][y] = (4 + rand.Int31n(5)) % 8
			}
		}
	}
}
